//: Playground - noun: a place where people can play

import Foundation

Float.random(in: 0..<1)
Float.random(in: 0..<1)

//final class ObjectPool<T> {
//
//    private var items: [T]
//    private let queue: DispatchQueue
//    private let semaphore: DispatchSemaphore
//
//    init(items: [T]) {
//        self.items = items
//        self.queue = DispatchQueue(label: "object.pool.worker.queue")
//        self.semaphore = DispatchSemaphore(value: items.count)
//    }
//
//    func acquire() -> T? {
//        var result: T?
//        if self.semaphore.wait(timeout: .distantFuture) == .success, !self.items.isEmpty {
//            self.queue.sync {
//                result = self.items.remove(at: 0)
//            }
//        }
//        return result
//    }
//
//    func release(_ item: T) {
//        self.queue.sync {
//            self.items.append(item)
//            self.semaphore.signal()
//        }
//    }
//}
//
//var queue = DispatchQueue(label: "workQ", attributes: .concurrent)
//var group = DispatchGroup()
//
//let pool = ObjectPool<String>.init(items: ["a", "b", "c"])
//let a = pool.acquire()
//let b = pool.acquire()
//let c = pool.acquire()
//
//queue.async(group: group) {
//    Thread.sleep(forTimeInterval: Double(2))
//    pool.release("d")
//}
//let d = pool.acquire()
//
//group.wait(timeout: .distantFuture)
