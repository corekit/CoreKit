//
//  CoreKit.swift
//  CoreKit
//
//  Created by Tibor Bödecs on 2018. 03. 30..
//  Copyright © 2018. Tibor Bödecs. All rights reserved.
//

@_exported import Foundation

#if canImport(Darwin)
@_exported import Darwin
#endif
#if canImport(Glibc)
@_exported import Glibc
#endif

#if canImport(AppKit)
@_exported import AppKit
#endif
#if canImport(UIKit)
@_exported import UIKit
#endif
#if canImport(WatchKit)
@_exported import WatchKit
#endif
